window.onload = init;

function init() {
  var imagens = document.querySelectorAll('img');

  for(var i = 0; i < imagens.length; i++) {
    imagens[i].addEventListener('mouseover', aumentarImagem);
    imagens[i].addEventListener('mouseout', diminuirImagem);
  }
}

function aumentarImagem() {
  this.classList.add('aumentarLivro');
}

function diminuirImagem() {
  this.classList.remove('aumentarLivro');
}
