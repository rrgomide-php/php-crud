<?php
  require('conexao.php');
  $sql =
    "SELECT
        L.codigo,
        L.categoria,
        C.categoria,
        L.titulo,
        L.autor,
        L.preco,
        L.imagem,
        L.qtde
    FROM
        livros L,
        categorias C
    WHERE
        L.categoria = C.codigo
    ORDER BY
        C.categoria
    ";

  $livros = $mysqli->query($sql);
  $linhas = $mysqli->affected_rows;
?>
