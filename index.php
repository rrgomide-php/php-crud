<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Cadastro de livros</title>
  <link href='./css/livros.css' type='text/css' rel='stylesheet'>
  <?php require('./php/livros.php'); ?>
</head>
<body>
  <table border='1' align='center'>
    <thead>
      <tr>
        <th>Imagem</th>
        <th>Categoria</th>
        <th>Título</th>
        <th>Autor</th>
        <th>Preço</th>
      </tr>
    </thead>
    <tbody>
      <?php
        for($i = 0; $i < $linhas; $i++) {
          $dados = $livros->fetch_assoc();
      ?>
        <tr>
          <td><center><img width='32px' height='48px' src="<?=$dados['imagem']?>"></center></td>
          <td><?=$dados['categoria']?></td>
          <td><?=$dados['titulo']?></td>
          <td><?=$dados['autor']?></td>
          <td><?='R$' . number_format($dados['preco'], 2, ',', '.')?></td>
        </tr>
      <?php
        }
      ?>
    </tbody>
  </table>
  <script src='./js/script.js'></script>
</body>
</html>
